import { useEffect, useState, memo, useCallback } from 'react';
import './App.css';

function Button(props) {
  console.count('render button');
  return <button {...props} style={{ backgroundColor: 'lightgray' }} />;
}
const MemoizedButton = memo(Button);

function ListItem({ children }) {
  console.count('render list item');
  return (
    <li>
      {children}
      <label style={{ fontSize: 'smaller' }}>
        <input type="checkbox" />
        Add to cart
      </label>
    </li>
  );
}

const MemoizedListItem = memo(ListItem);

function App() {
  const [searchString, setSearchString] = useState('');
  const [isSortingDesc, setSortingDesc] = useState(false);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    console.count('render fetch');
    fetch('https://reqres.in/api/products')
      .then((response) => response.json())
      .then((json) =>
        setProducts(
          json.data
            .filter((item) => item.name.includes(searchString))
            .sort((a, z) => (isSortingDesc ? z.name.localeCompare(a.name) : a.name.localeCompare(z.name))),
        ),
      );
  }, [searchString, isSortingDesc]);

  console.count('render app');

  const onClickButton = useCallback(() => {
    setSortingDesc((value) => !value);
  }, []);

  return (
    <div className="App">
      <input type="search" value={searchString} onChange={(e) => setSearchString(e.target.value)} />
      <MemoizedButton onClick={onClickButton}>Change sort direction</MemoizedButton>
      <ul>
        {products.map((product) => {
          return <MemoizedListItem key={product.id}>{product.name}</MemoizedListItem>;
        })}
      </ul>
    </div>
  );
}

export default App;
